import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
interface Fertilizer {
  
  id: number;
  fertilizer_name: string;
  fertilizer_description: string;
  price: number;
  imageUrl: string;
}
@Component({
  selector: 'app-fertilizers',
  templateUrl: './fertilizers.component.html',
  styleUrl: './fertilizers.component.css'
})
export class FertilizersComponent implements OnInit{
  fertilizers: Fertilizer[] = [];

  constructor(private service: ProductService) {}

  ngOnInit(): void {
    this.loadFertilizers();
  }

  loadFertilizers(): void {
    this.service.getAllFertilizers().subscribe((fertilizers: Fertilizer[]) => {
      this.fertilizers = fertilizers.map((fertilizer) => ({
        ...fertilizer,
        imageUrl: this.getImageUrlForfertilizer(fertilizer) 
      }));
    });
  }

  private getImageUrlForfertilizer(fertilizer: Fertilizer): string {
    const imageUrl = `assets/fertilizers/fertilizer${fertilizer.id}.webp`;
    console.log('Generated Image URL:', imageUrl);
    return imageUrl;
  }
  addToCart(fertilizer: Fertilizer): void {
    // Transform pebble into a product object
    const product: any = {
      fertilizer_name: fertilizer.fertilizer_name,
      fertilizer_description: fertilizer.fertilizer_description,
      price: fertilizer.price
    };
  
    // Add product to cartProducts array in localStorage
    const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
    cartProducts.push(product);
    localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  }
  
}


