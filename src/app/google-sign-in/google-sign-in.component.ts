import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';

@Component({
  selector: 'app-google-sign-in',
  templateUrl: './google-sign-in.component.html',
  styleUrl: './google-sign-in.component.css'
})
export class GoogleSignInComponent implements OnInit {
  
  user: SocialUser | undefined;
  GoogleLoginProvider = GoogleLoginProvider;
      customers: any;
      email:any;
      password: any;
     
  constructor(private router:Router, private readonly authService: SocialAuthService,private service:CustomerService) {}
  
  
   
  ngOnInit() {
      this.authService.authState.subscribe((user) => {
      this.user = user;
      if (user) {
          this.router.navigate(['home']);; 
        }
      });
  }
  
}
