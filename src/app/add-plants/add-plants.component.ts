import { Component, OnInit } from '@angular/core';

import { ProductService } from '../product.service';
declare var jQuery: any;

@Component({
  selector: 'app-add-plants',
  templateUrl: './add-plants.component.html',
  styleUrls: ['./add-plants.component.css'] 
})
export class AddPlantsComponent implements OnInit {
  id: any;
  plant_name: any;
  plant_description: any;
  price: any;
  editedPlant:any;
  isEditing: boolean = false;
  all_Plants:any[]=[];
  
  constructor(private service: ProductService ) {}

  ngOnInit(): void {
this.loadAllPlants();
  }
  loadAllPlants() {
    this.service.getAllPlants().subscribe((data: any[]) => {
      this.all_Plants = data;
    });
  }

  addPlant(form: any) {
    console.log(form);
    const plantData =[{
      plant_name: form.plant_name,
      plant_description: form.plant_description,
      price: form.price,
    }];

    console.log(plantData);
    this.service.registerPlant(plantData).subscribe((data: any) => {
      console.log(data);
      console.log(plantData);
    });
  }
  editPlant(plant: any): void {
    this.editedPlant = plant;
    this.isEditing = true;    
     jQuery('#editPlant').modal('show');
  }
  
  updatePlant(): void {
    const id = this.editedPlant.id;
    this.service.updatePlant(id, this.editedPlant).subscribe(data => {
      console.log(data);
      this.isEditing = false; 
    });
  }
  
  deletePlant(id: any) {
    this.service.deletePlant(id).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.all_Plants.findIndex((plant: any) => {
      return plant.id == id;
    });

    this.all_Plants.splice(i, 1);

  }


}

