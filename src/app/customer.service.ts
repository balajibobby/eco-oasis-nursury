import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  isUserLoggedIn:boolean;

  constructor(private client:HttpClient) {
    this.isUserLoggedIn=false

   }
   setIsUserLoggedIn(){
    this.isUserLoggedIn=true;
  }
  getIsUserLoggedIn():boolean{
    return this.isUserLoggedIn
  }
  setIsUserLoggedOut(){
    this.isUserLoggedIn=false;
  }
   registerCustomer(customerData: any): any {
    const url = "http://localhost:8085/register";
    console.log(customerData)
    console.log(url)
    return this.client.post(url, customerData)
  }
  getAllCustomers():any{
    return this.client.get("http://localhost:8085/getAll");
  }
 
  
}
