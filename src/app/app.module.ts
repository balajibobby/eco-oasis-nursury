import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { PlantsComponent } from './plants/plants.component';
import { FertilizersComponent } from './fertilizers/fertilizers.component';
import { AccessoriesComponent } from './accessories/accessories.component';
import { PotsComponent } from './pots/pots.component';
import { SeedsComponent } from './seeds/seeds.component';
import { AdminComponent } from './admin/admin.component';
import { AddPlantsComponent } from './add-plants/add-plants.component';
import { AddPotsComponent } from './add-pots/add-pots.component';
import { AddFertilizersComponent } from './add-fertilizers/add-fertilizers.component';
import { PebblesComponent } from './pebbles/pebbles.component';
import { GoogleSignInComponent } from './google-sign-in/google-sign-in.component';
import { SocialLoginModule, SocialAuthServiceConfig, GoogleLoginProvider, GoogleSigninButtonModule } from '@abacritt/angularx-social-login';
import { AddPebblesComponent } from './add-pebbles/add-pebbles.component';
import { AddAccessoriesComponent } from './add-accessories/add-accessories.component';
import { AddSeedsComponent } from './add-seeds/add-seeds.component';
import { CartComponent } from './cart/cart.component';
import { LogoutComponent } from './logout/logout.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { NgxPayPalModule } from 'ngx-paypal';
import { ReviewComponent } from './review/review.component';
import { OrderComponent } from './order/order.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
// import { AddToCartComponent } from './add-to-cart/add-to-cart.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    PlantsComponent,
    PotsComponent,
    FertilizersComponent,
    AccessoriesComponent,
    SeedsComponent,
    AdminComponent,
    AddPlantsComponent,
    AddPotsComponent,
    AddFertilizersComponent,
    PebblesComponent,
    GoogleSignInComponent,
    AddPebblesComponent,
    AddAccessoriesComponent,
    AddSeedsComponent,
    CartComponent,
    LogoutComponent,
    AboutComponent,
    FooterComponent,
    ReviewComponent,
    OrderComponent,
    CustomerListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    SocialLoginModule,
    GoogleSigninButtonModule,
    NgxPayPalModule 
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '1002921402793-hcg9a9k6lh2gn3ouir23q0qp1d5t0emn.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }