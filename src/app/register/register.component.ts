import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  registrationForm!: FormGroup;

  constructor(private fb: FormBuilder, private customerService: CustomerService) { }

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      dob: ['', Validators.required],
      mobile_no: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]], // Added pattern validation for mobile number
      address: ['', Validators.required],
      pincode: ['', [Validators.required, Validators.pattern('^[0-9]{6}$')]], // Added pattern validation for pincode
      email: ['', [Validators.required, Validators.email]], // Added email validation
      password: ['', Validators.required],
      confirm_password: ['', Validators.required],
    }, { validator: this.passwordMatchValidator }); // Added password matching validator
  }

  onSubmit() {
    if (this.registrationForm.valid) {
      const customerData = this.registrationForm.value;
      console.log(customerData)
      this.customerService.registerCustomer(customerData).subscribe(
       ( response:any) => {

          console.log('Registration successful:', response);
          
        },
        (error:any) => {
          console.error('Registration failed:', error);
          
        }
      );
    }
  }
  
  passwordMatchValidator(fg: FormGroup) {
    const passwordControl = fg.get('password');
    const confirmPasswordControl = fg.get('confirm_password');
  
    // Ensure controls are not null before accessing their values
    if (passwordControl && confirmPasswordControl) {
      const password = passwordControl.value;
      const confirmPassword = confirmPasswordControl.value;
  
      // Check if passwords match
      return password === confirmPassword ? null : { mismatch: true };
    }
  
    // Return null if controls are null (although this branch should never be reached)
    return null;
  }
  

}
