import { Component } from '@angular/core';
import { ProductService } from '../product.service';
declare var jQuery: any;
@Component({
  selector: 'app-add-fertilizers',
  templateUrl: './add-fertilizers.component.html',
  styleUrl: './add-fertilizers.component.css'
})
export class AddFertilizersComponent {
  fertilizer_name:any;
  fertilizer_description:any;
  price:any;
  editedFertilizer:any;
  isEditing: boolean = false;
  all_Fertilizers:any[]=[];
  constructor(private service: ProductService ) {}

  ngOnInit(): void {
    this.loadAllFertilizers();
  }
  loadAllFertilizers() {
    this.service.getAllFertilizers().subscribe((data: any[]) => {
      this.all_Fertilizers = data;
    });
  }

  addFertilizer(form: any) {
    console.log(form);
    const fertilizerData =[{
      fertilizer_name: form.fertilizer_name,
      fertilizer_description: form.fertilizer_description,
      price: form.price,
    }];

    console.log(fertilizerData);
    this.service.registerFertilizer(fertilizerData).subscribe((data: any) => {
      console.log(data);
      console.log(fertilizerData);
    });
  }
  editFertilizer(fertilizer: any): void {
    this.editedFertilizer = fertilizer;
    this.isEditing = true; 
    jQuery('#editFertilizer').modal('show');
  }
  
  updateFertilizer(): void {
    const id = this.editedFertilizer.id;
    this.service.updateFertilizer(id, this.editedFertilizer).subscribe(data => {
      console.log(data);
      this.isEditing = false; 
    });
  }
  
  deleteFertilizer(id: any) {
    this.service.deletePot(id).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.all_Fertilizers.findIndex((fertilizer: any) => {
      return fertilizer.id == id;
    });

    this.all_Fertilizers.splice(i, 1);

  }

}
