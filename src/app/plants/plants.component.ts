import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ProductService } from '../product.service';
interface Plant {
  id: number;
  plant_name: string;
  plant_description: string;
  price: number;
  imageUrl:string
}

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrl: './plants.component.css'
})
export class PlantsComponent implements OnInit {
  plants: Plant[]=[];
constructor(private service : ProductService){
  
}
  ngOnInit(): void {
    this.loadPlants();
  }
  loadPlants(): void {
    this.service.getAllPlants().subscribe((plants: Plant[]) => {
      this.plants = plants.map((plant) => ({
        ...plant,
        imageUrl: this.getImageUrlForPlant(plant) 
      }));
    });
  }
 
  private getImageUrlForPlant(plant: Plant): string {
    const imageUrl = `assets\\plants\\plant${plant.id}.webp`;
    console.log('Generated Image URL:', imageUrl);
    return imageUrl;
    
  }
  addToCart(plant: Plant): void {
    
    const product: any = {
      plant_name: plant.plant_name,
      plant_description: plant.plant_description,
      price: plant.price
    };
  
    
    const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
    cartProducts.push(product);
    localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  }

}
