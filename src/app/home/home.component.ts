import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

   items = [
    {
      title: 'Plants',
      description: 'Discover a diverse collection of healthy and vibrant plants for your garden. From flowering plants to lush greenery, our selection offers everything you need to create a thriving outdoor oasis. Browse through our extensive range and find the perfect plants to beautify your space.',
      image: 'assets/images/plants.jpg'
    },
    {
      title: 'Seeds',
      description: 'Kickstart your gardening journey with our premium quality seeds. Whether you\'re a seasoned gardener or a beginner, our wide range of seeds ensures you can grow your favorite plants with ease and success. Explore our selection and embark on your gardening adventure today!',
      image: 'assets/images/Seeds.jpg'
    },
    {
      title: 'Pots',
      description: 'Elevate your plants with our stylish and durable pots. Designed to complement any decor style, our pots come in various shapes, sizes, and materials, allowing you to showcase your plants in the best possible way. Enhance the beauty of your indoor or outdoor space with our stunning collection of pots.',
      image: 'assets/images/Pots.jpg'
    },
    {
      title: 'Fertilizers',
      description: 'Boost the health and growth of your plants with our premium fertilizers. Formulated with essential nutrients, our fertilizers promote strong roots, lush foliage, and vibrant blooms, ensuring your garden thrives all year round. Explore our range of fertilizers and nurture your plants to their full potential.',
      image: 'assets/images/fertilizers.jpg'
    },
    {
      title: 'Accessories',
      description: 'Enhance your gardening experience with our range of accessories and tools. From watering cans to gardening gloves, our high-quality accessories make gardening tasks easier and more enjoyable, so you can tend to your plants with care and precision. Discover innovative tools and accessories to streamline your gardening routine.',
      image: 'assets/images/Accessories.jpg'
    },
    {
      title: 'Pebbles',
      description: 'Add a touch of natural beauty to your garden with our collection of decorative pebbles. Whether you\'re creating a pathway, accenting a flower bed, or decorating a water feature, our pebbles will enhance the aesthetic appeal of your outdoor space. Available in various colors and sizes, our pebbles are durable and weather-resistant, ensuring long-lasting beauty for your landscape design.',
      image: 'assets/images/pebbles2.jpg'
    }
  ];
  
  
}
