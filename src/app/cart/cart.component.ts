import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
// import { IPayPalConfig } from 'ngx-paypal'; 
// import { NgxPayPalModule } from 'ngx-paypal';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  // @ViewChild('paypalButton', { static: false })
  // paypalButtonElement!: ElementRef;
  products:any[]=[];
  total:number=0;
  // paypalConfig: IPayPalConfig | undefined;
  // paypal: any;


 constructor(private router:Router){

 }
  ngOnInit(): void {
    this.loadCartProducts();
    this.calculateTotal();
      }
  loadCartProducts():void{
    const receivedProducts: string | null = localStorage.getItem("cartProducts"); // Corrected key name

    if(receivedProducts !== null){
      this.products=JSON.parse(receivedProducts);
    }
  }
  calculateTotal():void{
    this.total=this.products.reduce((total,product)=>total+product.price,0);
  }
  removeFromCart(index: number): void {
    this.products.splice(index, 1); // Remove the product from the array
    localStorage.setItem("cartProducts", JSON.stringify(this.products)); // Update localStorage
    this.calculateTotal(); // Recalculate total
  }
  buyNow(){
    this.router.navigate(['/order']);
  }
  feedback(){
    this.router.navigate(['/review']);
  }
  
 
}


