import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
interface Seed{
  id: number;
  seed_name: string;
  seed_description: string;
  price: number;
  imageUrl:string
}
@Component({
  selector: 'app-seeds',
  templateUrl: './seeds.component.html',
  styleUrl: './seeds.component.css'
})
export class SeedsComponent implements OnInit {
  seeds: Seed[]=[];
constructor(private service : ProductService){
  
}
  ngOnInit(): void {
    this.loadSeeds();
  }
  loadSeeds(): void {
    this.service.getAllSeeds().subscribe((seeds: Seed[]) => {
      this.seeds = seeds.map((seed) => ({
        ...seed,
        imageUrl: this.getImageUrlForPlant(seed) 
      }));
    });
  }
 
  private getImageUrlForPlant(seed: Seed): string {
    const imageUrl = `assets\\seeds\\seed${seed.id}.webp`;
    console.log('Generated Image URL:', imageUrl);
    return imageUrl;
    
  }
  addToCart(seed: Seed): void {
    // Transform pebble into a product object
    const product: any = {
      seed_name: seed.seed_name,
      seed_description: seed.seed_description,
      price: seed.price
    };
  
    // Add product to cartProducts array in localStorage
    const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
    cartProducts.push(product);
    localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  }

}
