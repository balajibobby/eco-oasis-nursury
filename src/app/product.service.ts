import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private client : HttpClient) { }
  getAllPlants():any{
    return this.client.get("http://localhost:8085/getAllPlants");
  }
  getAllPots():any{
    return this.client.get("http://localhost:8085/getAllPots");
  }
  getAllSeeds():any{
    return this.client.get("http://localhost:8085/getAllSeeds");
  }
  getAllFertilizers():any{
    return this.client.get("http://localhost:8085/getAllFertilizer");
  }
  getAllAccessories():any{
    return this.client.get("http://localhost:8085/getAllAccessories");
  }
  getAllPebbles():any{
    return this.client.get("http://localhost:8085/getAllPebbles");
  }
  registerPlant(plantData:any): any{
    return this.client.post("http://localhost:8085/savePlant",plantData);
  }
  registerPot(potData:any): any{
    return this.client.post("http://localhost:8085/savePot",potData);
  }
  registerFertilizer(fertilizerData:any): any{
    return this.client.post("http://localhost:8085/saveFertilizer",fertilizerData);
  }
  registerPebble(pebbleData:any): any{
    return this.client.post("http://localhost:8085/savePebble",pebbleData);
  }
  registerAccessory(accessoryData:any):any{
    return this.client.post("http://localhost:8085/saveAccessories",accessoryData);
  }
  registerSeed(seedData:any){
    return this.client.post("http://localhost:8085/saveSeed",seedData)
  }
  updatePlant(id: any, updatedPlant: any) {
    return this.client.put('http://localhost:8085/updatePlant', updatedPlant);
  }
  deletePlant(id: any): any {
    return this.client.delete('http://localhost:8085/deletePlant/' + id);
  }
  updatePebble(id:any,updatedPebble:any){
    return this.client.put('http://localhost:8085/updatePebble', updatedPebble);
  }
  deletePebble(id:any) :any{
    return this.client.delete('http://localhost:8085/deletePebble/'+id);
  }
  updatePot(id:any,updatedPot:any){
    return this.client.put('http://localhost:8085/updatePot', updatedPot);
  }
  deletePot(id:any) :any{
    return this.client.delete('http://localhost:8085/deletePot/'+id);
  }
  updateFertilizer(id:any,updatedFertilizer:any){
    return this.client.put('http://localhost:8085/updateFertilizer',updatedFertilizer);
  }
  deleteFertilizer(id:any) :any{
    return this.client.delete('http://localhost:8085/deleteFertilizer/'+id);
  }
  updateSeed(id:any,updatedSeed:any){
    return this.client.put('http://localhost:8085/updateSeed',updatedSeed);
  }
  deleteSeed(id:any){
    return this.client.delete('http://localhost:8085/deleteSeed/'+id);
  }
  updateAccessory(id:any,updatedAccessory:any){
    return this.client.put('http://localhost:8085/updateAccessory',updatedAccessory)
  }  
  deleteAccessory(id:any){
    return this.client.delete('http://localhost:8085/deleteAccessory/'+id);
  }
}
