import { Component } from '@angular/core';
import { ProductService } from '../product.service';
declare var jQuery: any;

@Component({
  selector: 'app-add-accessories',
  templateUrl: './add-accessories.component.html',
  styleUrl: './add-accessories.component.css'
})
export class AddAccessoriesComponent {
  id:any;
  accessory_name:any;
  accessory_description:any;
  price:any;
  editedAccessory:any;
  isEditing: boolean = false;
  all_Accessories:any[]=[];
  constructor(private service: ProductService ) {}

  ngOnInit(): void {
this.loadAllAccessories();
  }
  loadAllAccessories() {
    this.service.getAllAccessories().subscribe((data: any[]) => {
      this.all_Accessories = data;
    });
  }

  addAccessory(form: any) {
    console.log(form);
    const accessoryData =[{
      accessory_name: form.accessory_name,
      accessory_description: form.accessory_description,
      price: form.price,
    }];

    console.log(accessoryData);
    this.service.registerAccessory(accessoryData).subscribe((data: any) => {
      console.log(data);
      console.log(accessoryData);
    });
  }
  editAccessory (accessory: any): void {
    this.editedAccessory = accessory;
    this.isEditing = true; 
    jQuery('#editAccessory').modal('show');
  }
  
  updateAccessory(): void {
    const id = this.editedAccessory.id;
    this.service.updateAccessory(id, this.editedAccessory).subscribe(data => {
      console.log(data);
      this.isEditing = false; 
    });
  }
  
  deleteAccessory(id: any) {
    this.service.deleteAccessory(id).subscribe((data: any) => {
      console.log(data);
    });
  
    const i = this.all_Accessories.findIndex((seed: any) => {
      return seed.id == id;
    });
  
    this.all_Accessories.splice(i, 1);
  
  }

}
