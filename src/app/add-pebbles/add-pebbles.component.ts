import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
declare var jQuery: any;
@Component({
  selector: 'app-add-pebbles',
  templateUrl: './add-pebbles.component.html',
  styleUrl: './add-pebbles.component.css'
})
export class AddPebblesComponent implements OnInit{
  id:any;
  pebble_name:any;
  pebble_description:any;
  price:any;
  editedPebble:any;
  isEditing: boolean = false;
  all_Pebbles:any[]=[];
  constructor(private service: ProductService ) {}
  ngOnInit(): void {
    this.loadAllPebbles();
  }
  loadAllPebbles() {
    this.service.getAllPebbles().subscribe((data: any[]) => {
      this.all_Pebbles = data;
    });
  }

  addPebble(form: any) {
    console.log(form);
    const pebbleData =[{
      pebble_name: form.pebble_name,
      pebble_description: form.pebble_description,
      price: form.price,
      
    }];

    console.log(pebbleData);
    this.service.registerPebble(pebbleData).subscribe((data: any) => {
      console.log(data);
      console.log(pebbleData);
    });
  }
  editPebble(pebble: any): void {
    this.editedPebble = pebble;
    this.isEditing = true; 
    jQuery('#editPebble').modal('show');
  }
  
  updatePebble(): void {
    const id = this.editedPebble.id;
    this.service.updatePebble(id, this.editedPebble).subscribe(data => {
      console.log(data);
      this.isEditing = false; 
    });
  }
  
  deletePebble(id: any) {
    this.service.deletePebble(id).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.all_Pebbles.findIndex((pebble: any) => {
      return pebble.id == id;
    });

    this.all_Pebbles.splice(i, 1);
  }

}
