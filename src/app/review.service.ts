import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  //private apiUrl = 'http://localhost:8085/reviews'; // Your backend API URL

  constructor(private http: HttpClient) { }

  addReview(review: any){
    return this.http.post("http://localhost:8085/addReviews",review);
  }
  getReviews() {
    return this.http.get("http://localhost:8085/getAllReviews");
  }

 
}
