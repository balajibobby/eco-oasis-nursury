import { Component } from '@angular/core';
import { ProductService } from '../product.service';
declare var jQuery: any;
@Component({
  selector: 'app-add-seeds',
  templateUrl: './add-seeds.component.html',
  styleUrl: './add-seeds.component.css'
})
export class AddSeedsComponent {
  id:any;
  seed_name:any;
  seed_description:any;
  price:any;
  editedSeed:any;
  isEditing: boolean = false;
  all_Seeds:any[]=[];
  
  constructor(private service: ProductService ) {
    
  }

  ngOnInit(): void {
    this.loadAllSeeds();
  }
  loadAllSeeds() {
    this.service.getAllSeeds().subscribe((data: any[]) => {
      this.all_Seeds = data;
    });
  }

  addSeed(form: any) {
    console.log(form);
    const seedData =[{
      seed_name: form.seed_name,
      seed_description: form.seed_description,
      price: form.price,
    }];

    console.log(seedData);
    this.service.registerSeed(seedData).subscribe((data: any) => {
      console.log(data);
      console.log(seedData);
    });

}
editSeed (seed: any): void {
  this.editedSeed = seed;
  this.isEditing = true; // Set isEditing to true when editing starts
  jQuery('#editSeed').modal('show');
}

updateSeed(): void {
  const id = this.editedSeed.id;
  this.service.updateSeed(id, this.editedSeed).subscribe(data => {
    console.log(data);
    this.isEditing = false; 
  });
}

deleteSeed(id: any) {
  this.service.deleteSeed(id).subscribe((data: any) => {
    console.log(data);
  });

  const i = this.all_Seeds.findIndex((seed: any) => {
    return seed.id == id;
  });

  this.all_Seeds.splice(i, 1);

}
}
