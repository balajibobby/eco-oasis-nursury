import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'nursery';
  constructor(private router:Router){

  }
  isNavbar4Visible():boolean{
    const currentUrl = this.router.url;
    return currentUrl === '/home'||currentUrl === '/plants'||currentUrl === '/pots'||currentUrl === '/fertilzers'||currentUrl === '/seeds'||currentUrl === '/accessories' || currentUrl=== '/login';
  }
  isNavbar5Visible():boolean{
    const currentUrl = this.router.url;
    return currentUrl === '/add-plants'||currentUrl === '/add-seeds'||currentUrl === '/add-pots'||currentUrl === '/add-fertilzers'||currentUrl === '/add-accessories'||currentUrl === '/add-pebbles';


  }
}
