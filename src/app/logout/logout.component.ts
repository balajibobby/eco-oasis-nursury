import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
  constructor(private router:Router,private service:CustomerService){
    this.service.setIsUserLoggedOut();
    alert('loggedOut');
    this.router.navigate(['login']);
  }

}
