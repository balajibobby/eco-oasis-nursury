import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

interface Accessories {
  id: number;
  accessory_name: string;
  accessory_description: string;
  price: number;
  imageUrl: string;
}
@Component({
  selector: 'app-accessories',
  templateUrl: './accessories.component.html',
  styleUrl: './accessories.component.css'
})
export class AccessoriesComponent  implements OnInit{
  accessory: Accessories[] = [];

  constructor(private service: ProductService) {}

  ngOnInit(): void {
    this.loadAccessories();
  }

  loadAccessories(): void {
    this.service.getAllAccessories().subscribe((accessories: Accessories[]) => {
      this.accessory = accessories.map((accessory) => ({
        ...accessory,
        imageUrl: this.getImageUrlForPot(accessory) 
      }));
    });
  }

  private getImageUrlForPot(accessory: Accessories): string {
    const imageUrl = `assets/accessories/accessories${accessory.id}.webp`;
    console.log('Generated Image URL:', imageUrl);
    return imageUrl;
  }
  addToCart(accessory: Accessories): void {
    
    const product: any = {
      accessory_name: accessory.accessory_name,
      seed_description: accessory.accessory_description,
      price: accessory.price
    };
  
   
    const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
    cartProducts.push(product);
    localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  }

}
