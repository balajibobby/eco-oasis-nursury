import { Component } from '@angular/core';
import { ProductService } from '../product.service';


interface Pebble{
  "id":number;
  "pebble_name":string;
  "pebble_description":string;
  "price":number;
"imageUrl": string;
}
@Component({
  selector: 'app-pebbles',
  templateUrl: './pebbles.component.html',
  styleUrl: './pebbles.component.css'
})
export class PebblesComponent {
  pebbles: Pebble[]=[];
  constructor(private service : ProductService){
    
  }
    ngOnInit(): void {
      this.loadPebbles();
    }
    loadPebbles(): void {
      this.service.getAllPebbles().subscribe((pebbles: Pebble[]) => {
        this.pebbles = pebbles.map((pebble) => ({
          ...pebble,
          imageUrl: this.getImageUrlForPebble(pebble) 
          
        }));
        console.log(pebbles);
      });
     
    }
   
    private getImageUrlForPebble(pebble: Pebble): string {
      const imageUrl = `assets\\pebbles\\pebble${pebble.id}.webp`;
      console.log('Generated Image URL:', imageUrl);
      return imageUrl;
      
    }
    
    addToCart(pebble: Pebble): void {
      
      const product: any = {
        pebble_name: pebble.pebble_name,
        pebble_description: pebble.pebble_description,
        price: pebble.price
      };
    
      
      const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
      cartProducts.push(product);
      localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
    }
    

}
