import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { PlantsComponent } from './plants/plants.component';
import { PotsComponent } from './pots/pots.component';
import { AccessoriesComponent } from './accessories/accessories.component';
import { FertilizersComponent } from './fertilizers/fertilizers.component';
import { SeedsComponent } from './seeds/seeds.component';
import { AdminComponent } from './admin/admin.component';
import { AddPlantsComponent } from './add-plants/add-plants.component';
import { AddPotsComponent } from './add-pots/add-pots.component';
import { AddFertilizersComponent } from './add-fertilizers/add-fertilizers.component';
import { PebblesComponent } from './pebbles/pebbles.component';
import { AddPebblesComponent } from './add-pebbles/add-pebbles.component';
import { AddAccessoriesComponent } from './add-accessories/add-accessories.component';
import { AddSeedsComponent } from './add-seeds/add-seeds.component';
import { CartComponent } from './cart/cart.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { AboutComponent } from './about/about.component';
import { ReviewComponent } from './review/review.component';
import { OrderComponent } from './order/order.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
// import { AddToCartComponent } from './add-to-cart/add-to-cart.component';


const routes: Routes = [
  
  {path:'',component:HomeComponent,},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'plants',component:PlantsComponent},
  {path:'pots',component:PotsComponent},
  {path:'accessories',component:AccessoriesComponent},
  {path:'fertilizers',component:FertilizersComponent,},
  {path:'seeds',component:SeedsComponent,},
  {path:'pebbles',component:PebblesComponent,},
  {path:'admin',component:AdminComponent },
  {path:'addPlants',component:AddPlantsComponent},
  {path:'addPots',component:AddPotsComponent},
  {path:'addFertilizers',component:AddFertilizersComponent},
  {path:'addPebbles',component:AddPebblesComponent},
  {path:'addAccessories',component:AddAccessoriesComponent},
  {path:'addSeeds',component:AddSeedsComponent},
  // { path: 'shopping-cart', component: AddToCartComponent },
  {path:'cart',component:CartComponent,canActivate:[AuthGuard]},
  {path:'logout',component:LogoutComponent,canActivate:[AuthGuard]},
  {path:'about',component:AboutComponent},
  {path:'review',component:ReviewComponent},
  {path:'order',component:OrderComponent},
  {path:'list',component:CustomerListComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
