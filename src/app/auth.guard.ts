import { CanActivateFn,Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CustomerService } from './customer.service';
// export const authGuard: CanActivateFn = (route, state) => {
//   return true;
// };
@Injectable({
  // <-- This is important
  
providedIn: 'root'
})
export class AuthGuard {

  constructor(private service:CustomerService,private router:Router) {}

  canActivate(): boolean {

   
    if(this.service.getIsUserLoggedIn()){
      return true;
    }else{
      this.router.navigate(['login']);
      return false;
    }

  }
}