import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
export interface Customer {
  id: number;
  first_name: string;
  last_name: string;
  dob: Date;
  mobile_no: string;
  address: string;
  pincode: number;
  email: string;
  password: string;
  confirm_password: string;
}
@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrl: './customer-list.component.css'
})
export class CustomerListComponent {
  customers:any[]=[]
  constructor(private router : Router, private service:CustomerService){   }

  ngOnInit(): void {
    this.service.getAllCustomers().subscribe((data:any)=>{
      this.customers=data
    })
  }

}
