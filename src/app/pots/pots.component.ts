import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

interface Pot {
  
  id: number;
  pot_name: string;
  pot_description: string;
  price: number;
  imageUrl: string;
}

@Component({
  selector: 'app-pots',
  templateUrl: './pots.component.html',
  styleUrls: ['./pots.component.css']
})
export class PotsComponent implements OnInit {
  pots: Pot[] = [];

  constructor(private service: ProductService) {}

  ngOnInit(): void {
    this.loadPots();
  }

  loadPots(): void {
    this.service.getAllPots().subscribe((pots: Pot[]) => {
      this.pots = pots.map((pot) => ({
        ...pot,
        imageUrl: this.getImageUrlForPot(pot) 
      }));
    });
  }

  private getImageUrlForPot(pot: Pot): string {
    const imageUrl = `assets/pots/pot${pot.id}.webp`;
    console.log('Generated Image URL:', imageUrl);
    return imageUrl;
  }
   
  addToCart(pot: Pot): void {
    
    const product: any = {
      pot_name: pot.pot_name,
      pot_description: pot.pot_description,
      price: pot.price
    };
  
   
    const cartProducts: any[] = JSON.parse(localStorage.getItem("cartProducts") || "[]");
    cartProducts.push(product);
    localStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  }
}
