import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  customer:any;
  email_ID:any="";
  password:any="";

  constructor(private router : Router, private service:CustomerService){   }

  ngOnInit(): void {
    this.service.getAllCustomers().subscribe((data:any)=>{
      this.customer=data
    })
  }
  login(form:any){ 
    if(form.email_ID=="admin" && form.password=="admin"){
      alert("admin login sucess")
      this.router.navigate(['/admin']);
      return;
    }else{
      let loginSuccessful = false;
    for(let cus of this.customer){
    if(cus.email==form.email_ID && cus.password==form.password){
      loginSuccessful = true;
          this.service.setIsUserLoggedIn();
      alert(this.email_ID+" login success")
      console.log(cus);
      this.router.navigate([''])
      return;
    }
  }
  if (!loginSuccessful) {
    alert("Login failed");
  }
  }

}
}
