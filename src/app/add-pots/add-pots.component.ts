import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
declare var jQuery: any;
@Component({
  selector: 'app-add-pots',
  templateUrl: './add-pots.component.html',
  styleUrl: './add-pots.component.css'
})
export class AddPotsComponent implements OnInit  {
  id: any;
  pot_name: any;
  pot_description: any;
  price: any;
  editedPot:any;
  isEditing: boolean = false;
  all_Pots:any[]=[];
  constructor(private service: ProductService ) {}

  ngOnInit(): void {
    this.loadAllPots();
  }
  loadAllPots() {
    this.service.getAllPots().subscribe((data: any[]) => {
      this.all_Pots = data;
    });
  }
  addPot(form: any) {
    console.log(form);
    const potData =[{
      pot_name: form.pot_name,
      pot_description: form.pot_description,
      price: form.price,
    }];

    console.log(potData);
    this.service.registerPot(potData).subscribe((data: any) => {
      console.log(data);
      console.log(potData);
    });
  }
  editPot(pot: any): void {
    this.editedPot = {...pot};
    this.isEditing = true; 
    jQuery('#editPot').modal('show');
  }
  
  updatePot(): void {
    const id = this.editedPot.id;
    this.service.updatePot(id, this.editedPot).subscribe(data => {
      console.log(data);
      this.isEditing = false; 
    });
  }
  
  deletePot(id: any) {
    this.service.deletePot(id).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.all_Pots.findIndex((pot: any) => {
      return pot.id == id;
    });

    this.all_Pots.splice(i, 1);

  }
}

